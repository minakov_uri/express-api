// Build a server with Node's HTTP module
const http = require('http');
const port = 8080;

const requestHandler = (request, response) => {
    console.log(`URL: ${request.url}`);
    response.end('Hello, server!')
}

const server = http.createServer(requestHandler);

// Start the server
server.listen(port, (error) => {
    if (error) return console.log(`Error: ${error}`);
 
    console.log(`Server is listening on port ${port}`)
})